/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.Socket;
import static newpackage.Server.info;

public class Przesylanie extends Thread {

    DataInputStream dis;
    DataOutputStream dos;

    Przesylanie(Socket gniazdo) {
        try {
            dis = new DataInputStream(gniazdo.getInputStream());
            dos = new DataOutputStream(gniazdo.getOutputStream());
            System.out.println("FTP Client połączony ...");
            start();

        } catch (Exception ex) {
        }
    }

    void WysylaniePliku() throws Exception {
        String filename = dis.readUTF();

        File f = new File(info + "/" + filename);

        FileInputStream fin = new FileInputStream(f);
        int ch;
        do {
            ch = fin.read();
            dos.writeUTF(String.valueOf(ch));

        } while (ch != -1);
        fin.close();

    }

    public static void DeleteFile(String path) {
        try {

            File file = new File(path);

            if (file.delete()) {
                System.out.println(file.getName() + " zostal skasowany!");
            } else {
                System.out.println("Operacja kasowania sie nie powiodla.");
            }

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

    void OdbieraniePliku() throws Exception {
        String filename = dis.readUTF();

        File f = new File(info + "/" + filename);
        String option;

        if (f.exists()) {

            option = "Y";
            DeleteFile(info + "/" + filename);
        } else {

            option = "Y";
        }

        if (option.compareTo("Y") == 0) {
            FileOutputStream fout = new FileOutputStream(f);
            int ch;
            String temp;
            do {
                
                temp = dis.readUTF();
                ch = Integer.parseInt(temp);
                if (ch != -1) {
                    fout.write(ch);
                }
            } while (ch != -1);
            fout.close();
            final File folder = new File(info);
            listFilesForFolder(folder);
        } 

    }

    void Usun() throws Exception {

        String filename = dis.readUTF();

        DeleteFile(info + "/" + filename);

        final File folder = new File(info);
        listFilesForFolder(folder);

    }

    void Odswiez() throws Exception {

        final File folder = new File(info);
        listFilesForFolder(folder);

    }

    public void listFilesForFolder(final File folder) throws Exception {
        String s = "";
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {

                s = s + fileEntry.getName() + ':';

            }

        }
        dos.writeUTF(s);
    }

    public void run() {
        while (true) {
            try {
                System.out.println("Czekam na polecenie");
                String command = dis.readUTF();
                if (command.compareTo("POBIERZ") == 0) {
                    System.out.println("\tWysyłanie pliku");
                    WysylaniePliku();
                } else if (command.compareTo("WYSLIJ") == 0) {
                    System.out.println("\tPrzyjmowanie pliku");
                    OdbieraniePliku();
                } else if (command.compareTo("USUN") == 0) {
                    Usun();
                } else if (command.compareTo("ODSWIEZ") == 0) {
                    System.out.println("\tOdswiezam zawartosc servera ...");
                    Odswiez();
                } else if (command.compareTo("ROZŁACZ") == 0) {
                    System.out.println("\tWyłącznie servera");
                    System.exit(1);
                }
            } catch (Exception ex) {
            }
        }
    }
}