
package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.Socket;


class Client {

    private static DataInputStream dis;
    private static DataOutputStream dos;

    public static DataOutputStream getDos() {
        return dos;
    }

    Client(Socket gniazdo) {
        try {
            dis = new DataInputStream(gniazdo.getInputStream());
            dos = new DataOutputStream(gniazdo.getOutputStream());
        } catch (Exception ex) {
        }
    }

    static void WyslijPlik() throws Exception {

        String filename = FTPClient.getFile().getName();
        dos.writeUTF(filename);
        FileInputStream fin = new FileInputStream(FTPClient.getFile());
        int ch;
        do {
            ch = fin.read();
            dos.writeUTF(String.valueOf(ch));
        } while (ch != -1);
        fin.close();

        String ff = dis.readUTF();
        String fruits[] = ff.split(":");
        FTPClient.getjList2().setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = fruits;

            public int getSize() {
                return strings.length;
            }

            public String getElementAt(int i) {
                return strings[i];
            }
        });
        FTPClient.getjScrollPane3().setViewportView(FTPClient.getjList2());

    }

    static void Odbieranie() throws Exception {

        dos.writeUTF(FTPClient.getSelected());

        File f = new File(FTPClient.getSelected());

        FileOutputStream fout = new FileOutputStream(f);
        int ch;
        String temp;
        do {
            temp = dis.readUTF();
            ch = Integer.parseInt(temp);
            if (ch != -1) {
                fout.write(ch);
            }
        } while (ch != -1);
        fout.close();

    }

    static void Odswiez() throws Exception {
        String ff = dis.readUTF();
        String fruits[] = ff.split(":");
        FTPClient.getjList2().setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = fruits;

            public int getSize() {
                return strings.length;
            }

            public String getElementAt(int i) {
                return strings[i];
            }
        });
        FTPClient.getjScrollPane3().setViewportView(FTPClient.getjList2());

    }

    static void Usun() throws Exception {

        dos.writeUTF(FTPClient.getSelected());
        String ff = dis.readUTF();
        String fruits[] = ff.split(":"); 
        FTPClient.getjList2().setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = fruits;

            public int getSize() {
                return strings.length;
            }

            public String getElementAt(int i) {
                return strings[i];
            }
        });
        FTPClient.getjScrollPane3().setViewportView(FTPClient.getjList2());

    }

}