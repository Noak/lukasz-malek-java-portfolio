[LinkedIn Łukasz Małek] www.linkedin.com/in/lukaszmalek781

E-mail: lukaszmalek781@gmail.com

##PL

W repozytorium znajdują się dwa projekty w javie wykonane przeze mnie.
Pierwszy to server i klient FTP opary o sockety, natomiast drugi to system zarządzania schroniskiem dla zwierząt na komputery osobiste i urządzenia mobilne z dostępem do Web Service gdzie jest baza danych MySQL. Web Service generowany jest codziennie raport, który można pobierać w programie.   


##ENG

In the repository, there are two projects in Java made by me.
The first is server and FTP client fumes of sockets, while the second is a management system for shelter animals for personal computers and mobile devices with access to the Web service where the database is MySQL. Web Service is generated daily report that you can download the program.