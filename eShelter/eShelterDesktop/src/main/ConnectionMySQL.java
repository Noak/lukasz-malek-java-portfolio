/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;


public class ConnectionMySQL {
     Connection con=null;
   public Connection connectMySql(){
       try {
           //cargar nuestro driver
           Class.forName("com.mysql.jdbc.Driver");
           con=DriverManager.getConnection("jdbc:mysql://hidden:3306/shelter","hidden","hidden");
           
       } catch (ClassNotFoundException | SQLException e) {
           
           JOptionPane.showMessageDialog(null, "[Error Can't connect: check internet connection or mysql server] "+'\n'+e);
       }
       return con;
       
   }
}
