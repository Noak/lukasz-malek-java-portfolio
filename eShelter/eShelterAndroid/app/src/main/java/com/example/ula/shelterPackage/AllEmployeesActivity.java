package com.example.ula.shelterPackage;


import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Łukasz Małek
 */


public class AllEmployeesActivity extends ListActivity {



    private ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> employeesList;
    private SwipeRefreshLayout swipeContainer;

    private static String url_all_employees = "http://hidden/uploads/get_all_employees.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_EMPLOYEES = "employees";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_SURNAME = "surname";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_LOGIN = "login";
    private static final String TAG_STILLEMPLOYED = "stillemployed";
    private static final String TAG_ACCESS = "access";
    private static final String TAG_SIZE = "size";


    JSONArray employees = null;

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.all_employees);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        employeesList = new ArrayList<HashMap<String, String>>();


        new LoadAllEmployees().execute();
        ListView lv = getListView();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isOnline()) {
                    finish();
                    startActivity(getIntent());
                }
                else{
                    swipeContainer.setRefreshing(false);
                }
            }

        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String pid = ((TextView) view.findViewById(R.id.id)).getText()
                        .toString();
                String name = ((TextView) view.findViewById(R.id.name)).getText()
                        .toString();
                String surname = ((TextView) view.findViewById(R.id.surname)).getText()
                        .toString();
                String username = ((TextView) view.findViewById(R.id.username)).getText()
                        .toString();


                Intent in = new Intent(getApplicationContext(),
                        EditEmployeeActivity.class);
                // sending pid to next activity
                in.putExtra(TAG_ID, pid);
                in.putExtra(TAG_NAME, name);
                in.putExtra(TAG_SURNAME,surname);
                in.putExtra(TAG_USERNAME, username);

                startActivityForResult(in, 100);
                finish();
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }



    class LoadAllEmployees extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AllEmployeesActivity.this);
            pDialog.setMessage("Loading employees. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {

            HashMap<String, String> params = new HashMap<>();
            JSONObject json = jParser.makeHttpRequest(url_all_employees, "GET", params);
            try {

                int success = json.getInt(TAG_SUCCESS);
                int size=1;
                if (success == 1) {

                    employees = json.getJSONArray(TAG_EMPLOYEES);
                    for (int i = 0; i < employees.length(); i++) {
                        JSONObject c = employees.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NAME);
                        String surname = c.getString(TAG_SURNAME);
                        String login = c.getString(TAG_LOGIN);
                        String stillemployed = c.getString(TAG_STILLEMPLOYED);
                        String access = c.getString(TAG_ACCESS);
                        String yes="1";
                        String manager="1";

                        HashMap<String, String> map = new HashMap<String, String>();


                        map.put(TAG_ID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_SURNAME, surname);
                        map.put(TAG_LOGIN, login);
                        if(stillemployed.equals(yes)){
                            stillemployed="Yes";
                        }
                        else{
                            stillemployed="No";
                        }
                        map.put(TAG_STILLEMPLOYED, stillemployed);
                        if(access.equals(manager)){
                            access="Manager";
                        }
                        else{
                            access="Employed";
                        }
                        map.put(TAG_ACCESS, access);
                        map.put(TAG_SIZE, Integer.toString(size));
                        employeesList.add(map);
                        ++size;
                    }
                } else {
                    Intent i = new Intent(getApplicationContext(),
                            NewEmployeeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    ListAdapter adapter = new SimpleAdapter(
                            AllEmployeesActivity.this, employeesList,
                            R.layout.list_item_employess, new String[] { TAG_ID, TAG_SIZE,
                            TAG_NAME,TAG_SURNAME,TAG_LOGIN,TAG_STILLEMPLOYED,TAG_ACCESS},
                            new int[] { R.id.id,R.id.number, R.id.name,R.id.surname,R.id.username,R.id.stillEmp,R.id.access });
                    setListAdapter(adapter);
                }
            });

        }

    }


}
