package com.example.ula.shelterPackage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;

/**
 * Created by Łukasz Małek
 */

public class EditEmployeeActivity extends Activity {

    EditText txtName;
    EditText txtSurname;
    EditText txtUsername;
    Spinner txtStillEmployed;
    Spinner txtAccess;

    Button btnSave;
    Button btnDelete;
    Button btnRestart;

    String xid;
    String xname;
    String xsurname;
    String xusername;



    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();

    private static final String url_update_employee = "http://hidden/uploads/update_employed.php";

    private static final String url_delete_employee = "http://hidden/uploads/delete_employee.php";
    private static final String url_restart_password = "http://hidden/uploads/update_password_employee.php";
    private static final String TAG_SUCCESS = "success";

    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_SURNAME = "surname";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_LOGIN = "login";
    private static final String TAG_STILLEMPLOYED = "stillemployed";
    private static final String TAG_ACCESS = "access";
    private static final String TAG_PASSWORD = "password";



    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.edit_employee);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        txtName = (EditText) findViewById(R.id.inputNamee);
        txtSurname = (EditText) findViewById(R.id.inputSurnamee);
        txtUsername = (EditText) findViewById(R.id.inputUsernamee);
        txtStillEmployed = (Spinner) findViewById(R.id.spinnerStillEmp);
        txtAccess = (Spinner) findViewById(R.id.spinnerAccess);

        Intent i = getIntent();

        xid = i.getStringExtra(TAG_ID);
        xname = i.getStringExtra(TAG_NAME);
        xsurname = i.getStringExtra(TAG_SURNAME);
        xusername = i.getStringExtra(TAG_USERNAME);

        txtName.setText(xname);
        txtSurname.setText(xsurname);
        txtUsername.setText(xusername);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnRestart = (Button) findViewById(R.id.btnRestart);

        String[] items = new String[]{"Yes","No"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        txtStillEmployed.setAdapter(adapter);

        String[] items2 = new String[]{"Employee","Manager"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items2);
        txtAccess.setAdapter(adapter2);

        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
            if(isOnline()) {
                if (txtName.getText().toString().isEmpty() || txtSurname.getText().toString().isEmpty() || txtUsername.getText().toString().isEmpty() || txtStillEmployed.getSelectedItem().toString().isEmpty() || txtAccess.getSelectedItem().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Empty spaces!", Toast.LENGTH_LONG).show();
                } else {
                    String name = txtName.getText().toString();
                    String surname = txtSurname.getText().toString();
                    String login = txtUsername.getText().toString();
                    String stillEmployed = txtStillEmployed.getSelectedItem().toString();
                    String access = txtAccess.getSelectedItem().toString();
                    new SaveEmployeeDetails().execute(name, surname, login, stillEmployed, access);
                }
            }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (isOnline()) {
                    new DeleteEmployee().execute();
                }
            }
        });
        btnRestart.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(isOnline()) {
                    String password = "shelter777";
                    new RestartPassword().execute(password);
                    Toast.makeText(getApplicationContext(), "'shelter777' is a new password", Toast.LENGTH_SHORT).show();
                }
            }

        });

    }




    class SaveEmployeeDetails extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditEmployeeActivity.this);
            pDialog.setMessage("Saving employee ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            String name = args[0],
                    surname = args[1],
                    login = args[2],
                    stillemployed = args[3],
                    access = args[4];

            String stillEmp="Yes";
            String accessM="Manager";
            HashMap<String, String> params = new HashMap<>();
            params.put(TAG_ID, xid);
            params.put(TAG_NAME,name);
            params.put(TAG_SURNAME,surname);
            params.put(TAG_LOGIN,login);
            if(stillemployed.equals(stillEmp)){
                stillemployed="1";
            }
            else{
                stillemployed="0";
            }
            params.put(TAG_STILLEMPLOYED,stillemployed);
            if(access.equals(accessM)){
                access="1";
            }
            else{
                access="0";
            }
            params.put(TAG_ACCESS,access);

            JSONObject json = jsonParser.makeHttpRequest(url_update_employee,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    Intent si = new Intent(getApplicationContext(), AllEmployeesActivity.class);
                    startActivity(si);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to update employee!", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

    class RestartPassword extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditEmployeeActivity.this);
            pDialog.setMessage("Restart password...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            String password = args[0];

            HashMap<String, String> params = new HashMap<>();
            params.put(TAG_ID, xid);
            params.put(TAG_PASSWORD,password);

            JSONObject json = jsonParser.makeHttpRequest(url_restart_password,
                    "POST", params);


            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    Intent si = new Intent(getApplicationContext(), AllEmployeesActivity.class);
                    startActivity(si);

                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed restart password!", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }



    class DeleteEmployee extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditEmployeeActivity.this);
            pDialog.setMessage("Deleting employee ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            HashMap<String, String> params = new HashMap<>();
            params.put(TAG_ID, xid);
            JSONObject json = jsonParser.makeHttpRequest(url_delete_employee,
                    "POST", params);
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);

                    Intent si = new Intent(getApplicationContext(), AllEmployeesActivity.class);
                    startActivity(si);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to delete employee!", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }


}
