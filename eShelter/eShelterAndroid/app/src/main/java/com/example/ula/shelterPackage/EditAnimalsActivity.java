package com.example.ula.shelterPackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Random;

/**
 * Created by Łukasz Małek
 */

public class EditAnimalsActivity extends Activity {

    EditText txtName;
    EditText txtAge;
    EditText txtType;
    EditText txtSex;
    EditText txtHealth;

    Button btnSave;
    Button btnDelete;
    Button btnImage;

    String xid;
    String xname;
    String xage;
    String xtype;
    String xsex;
    String xhealth;
    private String nameForDB;
    private boolean photoOn=false;
    private static File file;
    private int random;
    private File picture=null;
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();





    private static final String url_update_animal = "http://hidden/uploads/update_animal.php";
    private static final String url_update_animal_with_imageadres = "http://hidden/uploads/update_animal_with_imageadres.php";
    private static final String url_delete_animal = "http://hidden/uploads/delete_animal.php";


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_AGE = "age";
    private static final String TAG_TYPE = "type";
    private static final String TAG_SEX = "sex";
    private static final String TAG_HEALTH = "health";
    private static final String TAG_IMAGEADRES = "imageadres";

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.edit_animal);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        txtName = (EditText) findViewById(R.id.inputNamee);
        txtAge = (EditText) findViewById(R.id.inputAgee);
        txtType = (EditText) findViewById(R.id.inputTypee);
        txtSex = (EditText) findViewById(R.id.inputSexx);
        txtHealth = (EditText) findViewById(R.id.inputHealthh);



        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnImage = (Button) findViewById(R.id.btnImage);

        if(LoginWindow.getAccess()==1){
            btnDelete.setEnabled(true);
        }

        Intent i = getIntent();

        xid = i.getStringExtra(TAG_ID);
        xname = i.getStringExtra(TAG_NAME);
        xage = i.getStringExtra(TAG_AGE);
        xtype = i.getStringExtra(TAG_TYPE);
        xsex = i.getStringExtra(TAG_SEX);
        xhealth = i.getStringExtra(TAG_HEALTH);

        txtName.setText(xname);
        txtAge.setText(xage);
        txtType.setText(xtype);
        txtSex.setText(xsex);
        txtHealth.setText(xhealth);

        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
             if(isOnline()) {
                 if (txtName.getText().toString().isEmpty() || txtAge.getText().toString().isEmpty() || txtType.getText().toString().isEmpty() || txtSex.getText().toString().isEmpty() || txtHealth.getText().toString().isEmpty()) {
                     Toast.makeText(getApplicationContext(), "Empty spaces!", Toast.LENGTH_LONG).show();
                 } else {
                     String name = txtName.getText().toString();
                     String age = txtAge.getText().toString();
                     String type = txtType.getText().toString();
                     String sex = txtSex.getText().toString();
                     String health = txtHealth.getText().toString();
                     if (photoOn == true) {

                         nameForDB = txtName.getText().toString();
                         new UploadFile().execute(file);
                         new SaveAnimalDetailsWithImageadres().execute(name, age, type, sex, health);
                     } else {

                         new SaveAnimalDetails().execute(name, age, type, sex, health);
                     }

                 }
             }
            }
        });


        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
              if(isOnline()) {
                  new DeleteAnimal().execute();
              }
            }

        });
        btnImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
              if(isOnline()) {
                  selectImage();
              }

            }

        });

    }

    private void selectImage() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(EditAnimalsActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))

                {
                    Intent takePic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    String namefile="pic_";
                    String jpg=".jpg";
                    File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    try {
                        picture = File.createTempFile(namefile,jpg,dir);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    takePic.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(picture));
                    startActivityForResult(takePic, 1);

                }

                else if (options[item].equals("Choose from Gallery"))

                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                }

                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }

            }

        });

        builder.show();

    }
    private Bitmap decodeFile(File f) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            final int REQUIRED_SIZE=500;
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
                    Bitmap myBitmap = decodeFile(picture);
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int x = size.x;
                    int y = size.y;
                    Bitmap scaledBitmap = Bitmap.createScaledBitmap(myBitmap, y, x, true);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);
                    File filesDir = getApplicationContext().getFilesDir();
                    File imageFile = new File(filesDir, txtName.getText().toString() + ".jpg");

                    OutputStream os;
                    try {
                        os = new FileOutputStream(imageFile);
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                        os.flush();
                        os.close();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Error writing bitmap "+e, Toast.LENGTH_LONG).show();

                    }
                    file = imageFile;
                    photoOn=true;
                }

                break;
            case 2:
                if(resultCode == RESULT_OK){



                    Uri selectedImage = data.getData();
                    String[] filePath = { MediaStore.Images.Media.DATA };
                    Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();

                    Bitmap bitmap = (BitmapFactory.decodeFile(picturePath));

                    File filesDir = getApplicationContext().getFilesDir();
                    File imageFile = new File(filesDir, txtName.getText().toString() + ".jpg");

                    OutputStream os;
                    try {
                        os = new FileOutputStream(imageFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                        os.flush();
                        os.close();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Error writing bitmap "+e, Toast.LENGTH_LONG).show();

                    }
                    file = imageFile;
                    photoOn=true;
                }
                break;
        }

    }


    class SaveAnimalDetails extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditAnimalsActivity.this);
            pDialog.setMessage("Saving animal ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            String name = args[0],
                age = args[1],
             type = args[2],
             sex = args[3],
             health = args[4];

            HashMap<String, String> params = new HashMap<>();
            params.put(TAG_ID, xid);
            params.put(TAG_NAME,name);
            params.put(TAG_AGE,age);
            params.put(TAG_TYPE,type);
            params.put(TAG_SEX,sex);
            params.put(TAG_HEALTH,health);


            JSONObject json = jsonParser.makeHttpRequest(url_update_animal,
                    "POST", params);


            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    Intent i = getIntent();
                    setResult(100, i);
                    Intent si = new Intent(getApplicationContext(), AllAnimalsActivity.class);
                    startActivity(si);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to update animal!", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }


    class SaveAnimalDetailsWithImageadres extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditAnimalsActivity.this);
            pDialog.setMessage("Saving animal ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            String name = args[0],
                    age = args[1],
                    type = args[2],
                    sex = args[3],
                    health = args[4];

            HashMap<String, String> params = new HashMap<>();
            params.put(TAG_ID, xid);
            params.put(TAG_NAME,name);
            params.put(TAG_AGE,age);
            params.put(TAG_TYPE,type);
            params.put(TAG_SEX,sex);
            params.put(TAG_HEALTH,health);
            params.put(TAG_IMAGEADRES,"schroniskojava.prv.pl/"+random+nameForDB.toString()+".jpg");

            JSONObject json = jsonParser.makeHttpRequest(url_update_animal_with_imageadres,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    Intent si = new Intent(getApplicationContext(), AllAnimalsActivity.class);
                    startActivity(si);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to update animal!", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

    class DeleteAnimal extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditAnimalsActivity.this);
            pDialog.setMessage("Deleting animal ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {


            HashMap<String, String> params = new HashMap<>();
            params.put(TAG_ID, xid);
            JSONObject json = jsonParser.makeHttpRequest(url_delete_animal,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);

                    Intent si = new Intent(getApplicationContext(), AllAnimalsActivity.class);
                    startActivity(si);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to delete animal!", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

    class UploadFile extends AsyncTask<File, String, String> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditAnimalsActivity.this);
            pDialog.setMessage("Sending picture to server..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(File... args) {
            FTPClient con;
            Random rand = new Random();
            random = rand.nextInt(1000000);

            try
            {
                con = new FTPClient();
                con.connect("schroniskojava.prv.pl");

                if (con.login("hidden", "hidden"))
                {
                    con.enterLocalPassiveMode(); // important!
                    con.setFileType(FTP.BINARY_FILE_TYPE);
                    FileInputStream in = new FileInputStream(args[0]);
                    boolean result = con.storeFile(random+nameForDB.toString()+".jpg", in);
                    in.close();
                    if (result) Log.v("upload result", "succeeded");
                    con.logout();
                    con.disconnect();
                }
            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(), "Error upload "+e, Toast.LENGTH_LONG).show();
            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }
}
