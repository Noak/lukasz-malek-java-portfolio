package com.example.ula.shelterPackage;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Łukasz Małek
 */

public class AllAnimalsActivityPic extends ListActivity {


    private ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> animalsList;
    private SwipeRefreshLayout swipeContainer;
    private static String url_all_animals = "http://hidden/uploads/get_all_animals.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_ANIMALS = "animals";
    private static final String TAG_NAME = "name";
    private static final String TAG_AGE = "age";
    private static final String TAG_TYPE = "type";
    private static final String TAG_SEX = "sex";
    private static final String TAG_HEALTH = "health";
    private static final String TAG_SIZE = "size";
    private static final String TAG_IMAGEADRES = "imageadres";

    JSONArray animals = null;


    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.all_animals);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);


        animalsList = new ArrayList<HashMap<String, String>>();
        new LoadAllAnimals().execute();
        ListView lv = getListView();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isOnline()) {
                    finish();
                    startActivity(getIntent());
                }
                else{
                    swipeContainer.setRefreshing(false);
                }
            }

        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String imageadres = ((TextView) view.findViewById(R.id.imageadres)).getText()
                        .toString();
                if(imageadres.isEmpty()){
                    Toast.makeText(getApplicationContext(),"There is no picture!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent in = new Intent(getApplicationContext(),
                            ShowPicture.class);
                    in.putExtra(TAG_IMAGEADRES, imageadres);
                    startActivityForResult(in, 100);
                    finish();
                }
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }


    class LoadAllAnimals extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AllAnimalsActivityPic.this);
            pDialog.setMessage("Loading animals. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {

            HashMap<String, String> params = new HashMap<>();
            JSONObject json = jParser.makeHttpRequest(url_all_animals, "GET", params);
            try {
                int success = json.getInt(TAG_SUCCESS);
                int size=1;
                if (success == 1) {
                    animals = json.getJSONArray(TAG_ANIMALS);
                    for (int i = 0; i < animals.length(); i++) {
                        JSONObject c = animals.getJSONObject(i);

                        String name = c.getString(TAG_NAME);
                        String age = c.getString(TAG_AGE);
                        String type = c.getString(TAG_TYPE);
                        String sex = c.getString(TAG_SEX);
                        String health = c.getString(TAG_HEALTH);
                        String imageadres = c.getString(TAG_IMAGEADRES);

                        HashMap<String, String> map = new HashMap<String, String>();

                        map.put(TAG_NAME, name);
                        map.put(TAG_AGE, age);
                        map.put(TAG_TYPE, type);
                        map.put(TAG_SEX, sex);
                        map.put(TAG_HEALTH, health);
                        map.put(TAG_SIZE, Integer.toString(size));
                        map.put(TAG_IMAGEADRES,imageadres);
                        animalsList.add(map);

                        ++size;
                    }
                } else {
                    Intent i = new Intent(getApplicationContext(),
                            NewAnimalsActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    ListAdapter adapter = new SimpleAdapter(
                            AllAnimalsActivityPic.this, animalsList,
                            R.layout.list_item, new String[] { TAG_SIZE,
                            TAG_NAME,TAG_AGE,TAG_TYPE,TAG_SEX,TAG_HEALTH,TAG_IMAGEADRES},
                            new int[] {R.id.number, R.id.name,R.id.age,R.id.type,R.id.sex,R.id.health,R.id.imageadres});
                    setListAdapter(adapter);

                }
            });

        }

    }
}
