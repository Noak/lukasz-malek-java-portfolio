package com.example.ula.shelterPackage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


import java.util.HashMap;

/**
 * Created by Łukasz Małek
 */

public class ChangePassword extends Activity{

    private EditText inputNewPssword;
    private EditText inputRepatPassword;
    private String password;
    JSONParser jsonParser = new JSONParser();
    private ProgressDialog pDialog;

    private static final String url_change_password = "http://hidden/uploads/update_password_employee.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_ID = "id";
    private static final String TAG_PASSWORD = "password";
    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.change_password);
        inputNewPssword = (EditText) findViewById(R.id.password);
        inputRepatPassword = (EditText) findViewById(R.id.repeatPassword);
        Button btnSave = (Button) findViewById(R.id.savePassword);



        btnSave.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    if (isOnline()) {
                        if (inputNewPssword.getText().toString().isEmpty() || inputRepatPassword.getText().toString().isEmpty()) {
                            Toast.makeText(getApplicationContext(), "Empty spaces!", Toast.LENGTH_LONG).show();
                        } else {

                            if (inputNewPssword.getText().toString().equals(inputRepatPassword.getText().toString())) {
                                password = inputNewPssword.getText().toString();
                                new SaveNewPassword().execute(LoginWindow.getId(), password);
                            } else {
                                Toast.makeText(getApplicationContext(), "Password are different!", Toast.LENGTH_LONG).show();
                            }


                        }


                    }
                }
            });
    }

    class SaveNewPassword extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ChangePassword.this);
            pDialog.setMessage("Password change ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            String id = args[0],
                     password= args[1];

            HashMap<String, String> params = new HashMap<>();
            params.put(TAG_ID, id);
            params.put(TAG_PASSWORD,password);

            JSONObject json = jsonParser.makeHttpRequest(url_change_password,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to change password!", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }




}
