package com.example.ula.shelterPackage;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
/**
 * Created by Łukasz Małek
 */

public class LoginWindow extends Activity {

    private ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    ArrayList<ArrayList<String>> employeesList;


    private static String url_all_employees = "http://hidden/uploads/get_all_employees_pass.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_EMPLOYEES = "employees";
    private static final String TAG_LOGIN = "login";
    private static final String TAG_PASSWORD = "password";
    private static final String TAG_ACCESS = "access";
    private static final String TAG_ID = "id";
    private EditText inputUsername;
    private String inputUsernames;
    private EditText inputPassword;
    private String inputPasswords;
    private boolean correctLogin=false;
    private static int access;
    private static String id;


    JSONArray employees = null;
    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    public static int getAccess(){

        return access;

    }

    public static String getId(){

        return id;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        inputUsername = (EditText) findViewById(R.id.login);
        inputPassword = (EditText) findViewById(R.id.password);
        Button btnViewMenu = (Button) findViewById(R.id.btnLogin);
        Button exit = (Button) findViewById(R.id.exitLogin);
        employeesList = new ArrayList<ArrayList<String>>();
        ImageView myImageView=(ImageView) findViewById(R.id.imageView4);
        myImageView.setImageResource(R.drawable.eshelter5);

        if (isOnline()){
            btnViewMenu.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    if(inputUsername.getText().toString().isEmpty() ||  inputPassword.getText().toString().isEmpty()){
                        Toast.makeText(getApplicationContext(), "Empty spaces!", Toast.LENGTH_LONG).show();
                    }
                    else{
                        inputUsernames=inputUsername.getText().toString();
                        inputPasswords=inputPassword.getText().toString();
                        new Login().execute();
                    }


                }
            });

            exit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    System.exit(0);

                }
            });
        }

        else {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();

            alertDialog.setTitle("Info");
            alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);

            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            });

            alertDialog.show();
        }

    }




    class Login extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginWindow.this);
            pDialog.setMessage("Checking data... Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            HashMap<String, String> params = new HashMap<>();
            JSONObject json = jParser.makeHttpRequest(url_all_employees, "GET", params);


            try {

                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    employees = json.getJSONArray(TAG_EMPLOYEES);

                    for (int i = 0; i < employees.length(); i++) {
                        JSONObject c = employees.getJSONObject(i);
                        ArrayList<String> list =new ArrayList<>();
                        String xid = c.getString(TAG_ID);
                        String login = c.getString(TAG_LOGIN);
                        String password = c.getString(TAG_PASSWORD);
                        String accessForUser = c.getString(TAG_ACCESS);

                        list.add(login);
                        list.add(password);
                        list.add(accessForUser);
                        list.add(xid);
                        employeesList.add(list);
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() { for(int i=0;i<employeesList.size();i++){
                    String log=employeesList.get(i).get(0);
                    String pass=employeesList.get(i).get(1);
                    String accessString=employeesList.get(i).get(2);
                    String idString=employeesList.get(i).get(3);

                    if(log.equals(inputUsernames) && pass.equals(inputPasswords) ){
                        Toast.makeText(getApplicationContext(),"Login successful", Toast.LENGTH_SHORT).show();
                        correctLogin=true;
                        access=Integer.parseInt(accessString);
                        id=idString;
                        Intent si = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(si);
                        employeesList = new ArrayList<ArrayList<String>>();
                        finish();
                    }

                }
                    if(correctLogin==false){
                        employeesList = new ArrayList<ArrayList<String>>();
                        Toast.makeText(getApplicationContext(),"Wrong username or password", Toast.LENGTH_SHORT).show();
                    }

                }
            });

        }

    }
}
