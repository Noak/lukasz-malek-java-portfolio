package com.example.ula.shelterPackage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;




/**
 * Created by Łukasz Małek
 */


public class NewEmployeeActivity extends Activity {


    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    EditText inputName;
    EditText inputSurname;
    EditText inputUsername;
    EditText inputPassword;
    EditText inputConfirmPassword;
    Spinner inputAccess;
    JSONParser jParser = new JSONParser();




    private static final String TAG_EMPLOYEES = "employees";
    private static final String TAG_LOGIN = "login";

    JSONArray employees = null;
    private static String url_all_employees = "http://hidden/uploads/get_all_employees_pass.php";
    private static String url_create_employee = "http://hidden/uploads/create_employee.php";


    private static final String TAG_SUCCESS = "success";
    private ArrayList<String> usernames=new ArrayList<>();

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.add_employee);


        new LoadAllEmployeesUsernames().execute();

        inputName = (EditText) findViewById(R.id.inputName);
        inputSurname = (EditText) findViewById(R.id.inputSurname);
        inputUsername = (EditText) findViewById(R.id.inputUsername);
        inputPassword = (EditText) findViewById(R.id.inputPassword);
        inputConfirmPassword = (EditText) findViewById(R.id.inputConfirmPassword);
        inputAccess  = (Spinner) findViewById(R.id.spinnerAccess);

        String[] items = new String[]{"Employee","Manager",};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
        inputAccess.setAdapter(adapter);
        Button btnCreateEmployee = (Button) findViewById(R.id.aCreateEmployee);

        btnCreateEmployee.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(isOnline()) {
                    if (inputName.getText().toString().isEmpty() || inputUsername.getText().toString().isEmpty() || inputSurname.getText().toString().isEmpty() || inputPassword.getText().toString().isEmpty() || inputConfirmPassword.getText().toString().isEmpty() || inputAccess.getSelectedItem().toString().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Empty spaces!", Toast.LENGTH_LONG).show();
                    } else {
                        String password = inputPassword.getText().toString();
                        String confirmPassword = inputConfirmPassword.getText().toString();

                        if (password.equals(confirmPassword)) {
                            String name = inputName.getText().toString();
                            String surname = inputSurname.getText().toString();
                            String username = inputUsername.getText().toString();
                            String access = inputAccess.getSelectedItem().toString();
                            String stillemployed = "1";
                            String flag = "1";

                            if (isExist() == true) {
                                Toast.makeText(getApplicationContext(), "Username already exists", Toast.LENGTH_LONG).show();
                            } else {
                                new CreateNewEmployee().execute(name, surname, username, confirmPassword, stillemployed, access, flag);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Passwords are different!", Toast.LENGTH_LONG).show();
                        }

                    }
                }
            }
        });


    }
    public boolean isExist(){
        boolean exist=false;
        for(int i=0;i<usernames.size();++i){
           String users= usernames.get(i);
            if(users.equals(inputUsername.getText().toString())){
                exist=true;

            }
        }
        return  exist;
    }


    class CreateNewEmployee extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(NewEmployeeActivity.this);
            pDialog.setMessage("Creating Employee..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            String name = args[0],
                    surname = args[1],
                    login = args[2],
                    password = args[3],
                    stillemployed = args[4],
                    access = args[5],
                    flag=args[6];

            HashMap<String, String> params = new HashMap<>();
            params.put("name", name);
            params.put("surname", surname);
            params.put("login", login);
            params.put("password", password);
            params.put("stillemployed", stillemployed);
            params.put("access", access);
            params.put("flag", flag);

            JSONObject json = jsonParser.makeHttpRequest(url_create_employee,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = new Intent(getApplicationContext(), AllEmployeesActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }

    class LoadAllEmployeesUsernames extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(NewEmployeeActivity.this);
            pDialog.setMessage("Loading usernames. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            HashMap<String, String> params = new HashMap<>();
            JSONObject json = jParser.makeHttpRequest(url_all_employees, "GET", params);



            try {
                int success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    employees = json.getJSONArray(TAG_EMPLOYEES);
                    for (int i = 0; i < employees.length(); i++) {
                        JSONObject c = employees.getJSONObject(i);

                        String login = c.getString(TAG_LOGIN);
                        usernames.add(login);
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }




}
