package com.example.ula.shelterPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Random;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Łukasz Małek
 */

public class NewAnimalsActivity extends Activity {

    private File picture=null;
    JSONParser jsonParser = new JSONParser();
    EditText inputName;
    EditText inputAge;
    EditText inputType;
    EditText inputSex;
    EditText inputHealth;
    private String name2;
    private boolean photoOn=false;
    private int random;
    private static String url_create_animal = "http://hidden/uploads/create_animal_without_imageadres.php";
    private static String url_create_animal_image = "http://hidden/uploads/create_animal.php";

    private static File file;
    private ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    private int size=0;
    private int space=0;



    private static String url_all_animals = "http://hidden/uploads/get_all_animals.php";
    private static String url_all_amountofspace = "http://hidden/uploads/get_all_amountofspace.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_SPACE = "space";
    private static final String TAG_ANIMALS = "animals";
    private static final String TAG_AMOUNTOFSPACE = "amountofspace";
    JSONArray animals = null;
    JSONArray animals2 = null;

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.add_animal);
        new CheckSize().execute();

        inputName = (EditText) findViewById(R.id.inputName);
        inputAge = (EditText) findViewById(R.id.inputAge);
        inputType = (EditText) findViewById(R.id.inputType);
        inputSex = (EditText) findViewById(R.id.inputSex);
        inputHealth = (EditText) findViewById(R.id.inputHealth);

        Button btnCreateAnimal = (Button) findViewById(R.id.btnCreateAnimal);
        Button btnAdd = (Button) findViewById(R.id.btnAdd);


        btnCreateAnimal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isOnline()) {
                    if (inputName.getText().toString().isEmpty() || inputAge.getText().toString().isEmpty() || inputType.getText().toString().isEmpty() || inputSex.getText().toString().isEmpty() || inputHealth.getText().toString().isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Empty spaces!", Toast.LENGTH_LONG).show();
                    } else {
                        String name = inputName.getText().toString();
                        String age = inputAge.getText().toString();
                        String type = inputType.getText().toString();
                        String sex = inputSex.getText().toString();
                        String health = inputHealth.getText().toString();
                        String flag = "1";

                        if (size == space) {
                            Toast.makeText(getApplicationContext(), "Shelter is full!", Toast.LENGTH_LONG).show();
                        } else {
                            if (photoOn == true) {

                                name2 = inputName.getText().toString();
                                new UploadFile().execute(file);
                                new CreateNewAnimalWithImage().execute(name, age, type, sex, health, flag);
                            } else {
                                new CreateNewAnimal().execute(name, age, type, sex, health, flag);
                            }
                        }


                    }
                }
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                selectImage();


            }

        });


    }



    private void selectImage() {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(NewAnimalsActivity.this);

        builder.setTitle("Add Photo!");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))

                {
                    Intent takePic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    String namefile="pic_";
                    String jpg=".jpg";
                    File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    try {
                        picture = File.createTempFile(namefile,jpg,dir);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    takePic.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(picture));
                    startActivityForResult(takePic, 1);


                }

                else if (options[item].equals("Choose from Gallery"))

                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                }

                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }

            }

        });

        builder.show();

    }
    private Bitmap decodeFile(File f) {
        try {

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            final int REQUIRED_SIZE=500;
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
                    Bitmap myBitmap = decodeFile(picture);
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    int x = size.x;
                    int y = size.y;


                    Bitmap scaledBitmap = Bitmap.createScaledBitmap(myBitmap, y, x, true);
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap , 0, 0, scaledBitmap .getWidth(), scaledBitmap .getHeight(), matrix, true);


                    File filesDir = getApplicationContext().getFilesDir();
                    File imageFile = new File(filesDir, inputName.getText().toString() + ".jpg");

                    OutputStream os;
                    try {
                        os = new FileOutputStream(imageFile);
                        rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                        os.flush();
                        os.close();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Error writing bitmap "+e, Toast.LENGTH_LONG).show();

                    }

                    file = imageFile;
                    photoOn=true;
                }

                break;
            case 2:
                if(resultCode == RESULT_OK){



                    Uri selectedImage = data.getData();
                    String[] filePath = { MediaStore.Images.Media.DATA };
                    Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();
                    Bitmap bitmap = (BitmapFactory.decodeFile(picturePath));

                    File filesDir = getApplicationContext().getFilesDir();
                    File imageFile = new File(filesDir, inputName.getText().toString() + ".jpg");

                    OutputStream os;
                    try {
                        os = new FileOutputStream(imageFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                        os.flush();
                        os.close();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "Error writing bitmap "+e, Toast.LENGTH_LONG).show();

                    }
                    file = imageFile;
                }
                break;
        }

    }


    class CreateNewAnimal extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(NewAnimalsActivity.this);
            pDialog.setMessage("Creating Animals..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            String name = args[0],
                    age = args[1],
                    type = args[2],
                    sex = args[3],
                    health = args[4],
                    flag = args[5];

            HashMap<String, String> params = new HashMap<>();
            params.put("name", name);
            params.put("age", age);
            params.put("type", type);
            params.put("sex", sex);
            params.put("health", health);
            params.put("flag", flag);

            JSONObject json = jsonParser.makeHttpRequest(url_create_animal,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = new Intent(getApplicationContext(), AllAnimalsActivity.class);
                    startActivity(i);

                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }

    class CreateNewAnimalWithImage extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(NewAnimalsActivity.this);
            pDialog.setMessage("Creating Animals..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            String name = args[0],
                    age = args[1],
                    type = args[2],
                    sex = args[3],
                    health = args[4],
                    flag = args[5];

            HashMap<String, String> params = new HashMap<>();
            params.put("name", name);
            params.put("age", age);
            params.put("type", type);
            params.put("sex", sex);
            params.put("health", health);
            params.put("flag", flag);
            params.put("imageadres","schroniskojava.prv.pl/"+random+name2+".jpg");

            JSONObject json = jsonParser.makeHttpRequest(url_create_animal_image,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = new Intent(getApplicationContext(), AllAnimalsActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }

    class UploadFile extends AsyncTask<File, String, String> {



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(NewAnimalsActivity.this);
            pDialog.setMessage("Sending picture to server..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(File... args) {



            FTPClient con;
            Random rand = new Random();
            random = rand.nextInt(1000000);

            try
            {
                con = new FTPClient();
                con.connect("schroniskojava.prv.pl");

                if (con.login("hidden", "hidden"))
                {
                    con.enterLocalPassiveMode();
                    con.setFileType(FTP.BINARY_FILE_TYPE);

                    FileInputStream in = new FileInputStream(args[0]);
                    boolean result = con.storeFile(random+name2.toString()+".jpg", in);
                    in.close();
                    if (result) Log.v("upload result", "succeeded");
                    con.logout();
                    con.disconnect();
                }
            }
            catch (Exception e)
            {
                Toast.makeText(getApplicationContext(), "Error upload "+e, Toast.LENGTH_LONG).show();
            }





            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }



    class CheckSize extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(NewAnimalsActivity.this);
            pDialog.setMessage("Check Size.. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            HashMap<String, String> params = new HashMap<>();
            JSONObject json = jParser.makeHttpRequest(url_all_amountofspace, "GET", params);



            try {
                int success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    animals = json.getJSONArray(TAG_AMOUNTOFSPACE);
                    for (int i = 0; i < animals.length(); i++) {
                        JSONObject c = animals.getJSONObject(i);
                        String getSpace = c.getString(TAG_SPACE);
                        space=Integer.parseInt(getSpace);

                    }
                    HashMap<String, String> params2 = new HashMap<>();
                    JSONObject json2 = jParser.makeHttpRequest(url_all_animals, "GET", params2);



                    try {
                        int success2 = json2.getInt(TAG_SUCCESS);
                        if (success2 == 1) {
                            animals2 = json2.getJSONArray(TAG_ANIMALS);
                            for (int i = 0; i < animals2.length(); i++) {
                                ++size;
                            }
                        } else {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();

        }

    }

}
