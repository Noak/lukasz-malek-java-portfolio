package com.example.ula.shelterPackage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;


/**
 * Created by Łukasz Małek
 */

public class EditPlacesAndEmail extends Activity{

    private static String url_all_amountofspace = "http://hidden/uploads/get_all_amountofspace.php";
    private static String url_update_amountofspace = "http://hidden/uploads/update_spacemail_amountofspace.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_AMOUNTOFSPACE = "amountofspace";
    private static final String TAG_SPACE = "space";
    private static final String TAG_EMAIL = "email";
    JSONParser jParser = new JSONParser();
    JSONParser jsonParser = new JSONParser();
    private ProgressDialog pDialog;
    JSONArray amountOfSpace = null;
    String email;
    String space;

    EditText txtSpace;
    EditText txtEmail;
    Button btnSaveEmailAndSpace;

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.edit_email_space);
        txtSpace = (EditText) findViewById(R.id.space);
        txtEmail = (EditText) findViewById(R.id.email);
        new LoadSpaceEmail().execute();
        btnSaveEmailAndSpace= (Button) findViewById(R.id.saveEmailAndSpace);

        btnSaveEmailAndSpace.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
            if(isOnline()) {
                if (txtSpace.getText().toString().isEmpty() || txtEmail.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Empty spaces!", Toast.LENGTH_LONG).show();
                } else {
                    String getSpace = txtSpace.getText().toString();
                    String getEmail = txtEmail.getText().toString();

                    new SaveSpaceAndEmail().execute(getSpace, getEmail);

                }
            }
            }
        });



    }

    class LoadSpaceEmail extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditPlacesAndEmail.this);
            pDialog.setMessage("Loading space and email... Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            HashMap<String, String> params = new HashMap<>();
            JSONObject jsonSpace = jParser.makeHttpRequest(url_all_amountofspace, "GET", params);



            try {

                amountOfSpace = jsonSpace.getJSONArray(TAG_AMOUNTOFSPACE);
                for (int i = 0; i < amountOfSpace.length(); i++) {
                    JSONObject c = amountOfSpace.getJSONObject(i);
                    space = c.getString(TAG_SPACE);
                    email = c.getString(TAG_EMAIL);


                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    txtSpace.setText(space);
                    txtEmail.setText(email);

                }
            });
            }


        }

    class SaveSpaceAndEmail extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditPlacesAndEmail.this);
            pDialog.setMessage("Save space and email ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            String space = args[0],
                    email = args[1];


            HashMap<String, String> params = new HashMap<>();
            params.put(TAG_SPACE, space);
            params.put(TAG_EMAIL,email);

            JSONObject json = jsonParser.makeHttpRequest(url_update_amountofspace,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to update", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

}
