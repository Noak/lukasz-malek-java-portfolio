package com.example.ula.shelterPackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;
import com.opencsv.CSVWriter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Łukasz Małek
 */

public class MainActivity extends Activity{

    Button btnViewAnimals;
    Button btnViewAnimalsPic;
    Button btnNewAnimal;
    Button btnLogout;
    Button btnViewEmployess;
    Button btnNewEmployee;
    Button btnChangePassword;
    Button  btnCreateCsvFile;
    Button btnEditSpaceAndEmail;
    Button btnReport;
    private static int access=LoginWindow.getAccess();
    private static String loginUserId=LoginWindow.getId();
    private ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> animalsList;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_ANIMALS = "animals";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_AGE = "age";
    private static final String TAG_TYPE = "type";
    private static final String TAG_SEX = "sex";
    private static final String TAG_HEALTH = "health";
    private static String url_all_animals = "http://hidden/uploads/get_all_animals.php";
    JSONArray animals = null;
    CSVWriter writer;

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    public static int getAccess(){

        return access;

    }

    public static String getUserId(){

        return loginUserId;


    }

    private void logoutUser() {

        Intent intent = new Intent(MainActivity.this,  LoginWindow.class);
        startActivity(intent);
        finish();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        animalsList = new ArrayList<HashMap<String, String>>();

        btnViewAnimals = (Button) findViewById(R.id.btnViewAnimals);
        btnViewAnimalsPic = (Button) findViewById(R.id.btnViewAnimalsPic);
        btnNewAnimal = (Button) findViewById(R.id.btnCreateAnimal);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnViewEmployess = (Button) findViewById(R.id.btnViewAllEmployees);
        btnNewEmployee = (Button) findViewById(R.id.btnCreateEmployee);
        btnChangePassword = (Button) findViewById(R.id.btnChangePassword);
        btnCreateCsvFile= (Button) findViewById(R.id.createCsv);
        btnEditSpaceAndEmail= (Button) findViewById(R.id.editSpaceAndEmail);
        btnReport= (Button) findViewById(R.id.report);


        if(LoginWindow.getAccess()==1){
            btnViewEmployess.setEnabled(true);
            btnNewEmployee.setEnabled(true);
            btnEditSpaceAndEmail.setEnabled(true);
        }
        else{
            btnViewEmployess.setEnabled(false);
            btnNewEmployee.setEnabled(false);
            btnEditSpaceAndEmail.setEnabled(false);
        }


        btnViewAnimals.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isOnline()) {
                    Intent i = new Intent(getApplicationContext(), AllAnimalsActivity.class);
                    startActivity(i);
                }
            }
        });

        btnViewAnimalsPic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isOnline()) {
                    Intent i = new Intent(getApplicationContext(), AllAnimalsActivityPic.class);
                    startActivity(i);
                }

            }
        });

        btnNewAnimal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isOnline()) {
                    Intent i = new Intent(getApplicationContext(), NewAnimalsActivity.class);
                    startActivity(i);
                }

            }
        });

        btnViewEmployess.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isOnline()) {
                    Intent i = new Intent(getApplicationContext(), AllEmployeesActivity.class);
                    startActivity(i);
                }

            }
        });

        btnNewEmployee.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isOnline()) {
                    Intent i = new Intent(getApplicationContext(), NewEmployeeActivity.class);
                    startActivity(i);
                }

            }
        });
        btnChangePassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isOnline()) {
                    Intent i = new Intent(getApplicationContext(), ChangePassword.class);
                    startActivity(i);
                }

            }
        });
        btnLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                logoutUser();

            }
        });

        btnCreateCsvFile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isOnline()) {
                    new CreateFileCsv().execute();
                }

            }
        });

        btnReport.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isOnline()) {
                    new DownloadReport().execute();
                }
            }
        });
        btnEditSpaceAndEmail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (isOnline()) {
                    Intent i = new Intent(getApplicationContext(), EditPlacesAndEmail.class);
                    startActivity(i);
                }

            }
        });
    }

    class CreateFileCsv extends AsyncTask<String, String, String>  {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Create file csv...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            HashMap<String, String> params = new HashMap<>();
            JSONObject json = jParser.makeHttpRequest(url_all_animals, "GET", params);



            try {

                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    animals = json.getJSONArray(TAG_ANIMALS);


                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String date = df.format(Calendar.getInstance().getTime());
                    String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
                    String fileName = "ShelterTable"+date+".csv";
                    String filePath = baseDir + File.separator + fileName;
                    File f = new File(filePath );

                    FileWriter mFileWriter;
                    if(f.exists() && !f.isDirectory()){


                        try {
                            mFileWriter = new FileWriter(filePath , true);
                            writer = new CSVWriter(mFileWriter);
                        }catch(IOException ex) {
                            Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_LONG).show();
                        }



                    }
                    else {
                        try {
                            writer = new CSVWriter(new FileWriter(filePath));
                        }catch(IOException ex) {
                            Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_LONG).show();
                        }


                    }

                    int size=1;
                    String[] data2 = {"No.","Name","Age:","Type:","Sex:","Health:"};

                    writer.writeNext(data2);
                    for (int i = 0; i < animals.length(); i++) {
                        JSONObject c = animals.getJSONObject(i);


                        String name = c.getString(TAG_NAME);
                        String age = c.getString(TAG_AGE);
                        String type = c.getString(TAG_TYPE);
                        String sex = c.getString(TAG_SEX);
                        String health = c.getString(TAG_HEALTH);

                        String[] data = {Integer.toString(size),name,age,type,sex,health};
                        writer.writeNext(data);
                        ++size;
                    }
                    try {
                        writer.close();

                    }catch(IOException ex) {
                        Toast.makeText(getApplicationContext(), "Failed!", Toast.LENGTH_LONG).show();
                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(),"The file is located in the main directory", Toast.LENGTH_SHORT).show();
        }

    }

    class DownloadReport extends AsyncTask<String, String, String>  {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Download Report...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {




            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String date = df.format(Calendar.getInstance().getTime());

            int count;
            try {
                String root = Environment.getExternalStorageDirectory().toString();
                URL url = new URL("http://hidden/ftp/Report.pdf");

                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();


                InputStream input = new BufferedInputStream(url.openStream(), 8192);
                OutputStream output = new FileOutputStream(root+"/Report"+date+".pdf");
                byte data[] = new byte[1024];

                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    output.write(data, 0, count);

                }
                output.flush();
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;

        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            Toast.makeText(getApplicationContext(),"The file is located in the main directory", Toast.LENGTH_SHORT).show();
        }

    }


}
