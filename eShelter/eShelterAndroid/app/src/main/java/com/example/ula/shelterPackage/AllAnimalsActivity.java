package com.example.ula.shelterPackage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;


import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by Łukasz Małek
 */

public class AllAnimalsActivity extends ListActivity{

    // Progress Dialog
    private ProgressDialog pDialog;
    private String email;
    private int space=0;
    private int size2=0;
    private String isSendMail;

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> animalsList;

    // url to get all animals list
    private static String url_all_animals = "http://hidden/uploads/get_all_animals.php";
    private static String url_all_amountofspace = "http://hidden/uploads/get_all_amountofspace.php";
    private static String url_flag_mail = "http://hidden/uploads/get_flag_mail.php";
    private static String url_update_flag_mail = "http://hidden/uploads/update_mail_flag.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_ANIMALS = "animals";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_AGE = "age";
    private static final String TAG_TYPE = "type";
    private static final String TAG_SEX = "sex";
    private static final String TAG_HEALTH = "health";
    private static final String TAG_SIZE = "size";
    private static final String TAG_AMOUNTOFSPACE = "amountofspace";
    private static final String TAG_MAIL = "mail";
    private static final String TAG_FLAG = "flag";
    private static final String TAG_SPACE = "space";
    private static final String TAG_EMAIL = "email";


    // animals JSONArray
    JSONArray animals = null;
    JSONArray amountOfSpace = null;
    JSONArray flagMail = null;
    JSONParser jsonParser = new JSONParser();
    private SwipeRefreshLayout swipeContainer;

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            Toast.makeText(getApplicationContext(), "No Internet connection!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.all_animals);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Hashmap for ListView
        animalsList = new ArrayList<HashMap<String, String>>();

        // Loading animals in Background Thread
        new LoadAllAnimals().execute();

        // Get listview
        ListView lv = getListView();


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isOnline()) {
                    finish();
                    startActivity(getIntent());
                }
                else{
                    swipeContainer.setRefreshing(false);
                }
            }

        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        // on seleting single animal
        // launching Edit Animal Screen
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                // getting values from selected ListItem
                String pid = ((TextView) view.findViewById(R.id.id)).getText()
                        .toString();
                String name = ((TextView) view.findViewById(R.id.name)).getText()
                        .toString();
                String age = ((TextView) view.findViewById(R.id.age)).getText()
                        .toString();
                String type = ((TextView) view.findViewById(R.id.type)).getText()
                        .toString();
                String sex = ((TextView) view.findViewById(R.id.sex)).getText()
                        .toString();
                String health = ((TextView) view.findViewById(R.id.health)).getText()
                        .toString();

                // Starting new intent
                Intent in = new Intent(getApplicationContext(),
                        EditAnimalsActivity.class);
                // sending pid to next activity
                in.putExtra(TAG_ID, pid);
                in.putExtra(TAG_NAME, name);
                in.putExtra(TAG_AGE, age);
                in.putExtra(TAG_TYPE, type);
                in.putExtra(TAG_SEX, sex);
                in.putExtra(TAG_HEALTH, health);



                // starting new activity and expecting some response back
                startActivityForResult(in, 100);
                finish();
            }
        });

    }



    // Response from Edit Animal Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if result code 100
        if (resultCode == 100) {
            // if result code 100 is received
            // means user edited/deleted animal
            // reload this screen again
            Intent intent = getIntent();
            startActivity(intent);
            finish();

        }

    }




    /**
     * Background Async Task to Load all animals by making HTTP Request
     * */
    class LoadAllAnimals extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AllAnimalsActivity.this);
            pDialog.setMessage("Loading animals. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All animals from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters
            HashMap<String, String> params = new HashMap<>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_animals, "GET", params);



            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);
                int size=1;
                if (success == 1) {
                    // animals found
                    // Getting Array of Animals
                    animals = json.getJSONArray(TAG_ANIMALS);

                    // looping through All Animals
                    for (int i = 0; i < animals.length(); i++) {
                        JSONObject c = animals.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(TAG_ID);
                        String name = c.getString(TAG_NAME);
                        String age = c.getString(TAG_AGE);
                        String type = c.getString(TAG_TYPE);
                        String sex = c.getString(TAG_SEX);
                        String health = c.getString(TAG_HEALTH);



                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        map.put(TAG_ID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_AGE, age);
                        map.put(TAG_TYPE, type);
                        map.put(TAG_SEX, sex);
                        map.put(TAG_HEALTH, health);
                        map.put(TAG_SIZE, Integer.toString(size));


                        // adding HashList to ArrayList
                        animalsList.add(map);
                        ++size;
                        ++size2;
                    }

                } else {
                    // no animals found
                    // Launch Add New animals Activity
                    Intent i = new Intent(getApplicationContext(),
                            NewAnimalsActivity.class);
                    // Closing all previous activities
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
                HashMap<String, String> params2 = new HashMap<>();
                // getting JSON string from URL
                JSONObject jsonSpace = jParser.makeHttpRequest(url_all_amountofspace, "GET", params2);
                amountOfSpace = jsonSpace.getJSONArray(TAG_AMOUNTOFSPACE);

                // looping through amount of space
                for (int i = 0; i < amountOfSpace.length(); i++) {
                    JSONObject c = amountOfSpace.getJSONObject(i);

                    // Storing each json item in variable

                    String getSpace = c.getString(TAG_SPACE);
                    email = c.getString(TAG_EMAIL);
                    space=Integer.parseInt(getSpace);





                }
                JSONObject jsonMail = jParser.makeHttpRequest(url_flag_mail, "GET", params2);


                flagMail = jsonMail.getJSONArray(TAG_MAIL);

                // looping through flag mail
                for (int i = 0; i < flagMail.length(); i++) {
                    JSONObject c = flagMail.getJSONObject(i);

                    // Storing each json item in variable

                    isSendMail = c.getString(TAG_FLAG);

                }

                String notSend="0";
                String setflagMail=isSendMail;
                if(size2<space-5){
                    setflagMail="0";
                }



                if(size2>space-5 && isSendMail.equals(notSend)){
                    final String username = "schroniskojava@gmail.com";
                    final String password = "hidden";

                    Properties props = new Properties();
                    props.put("mail.smtp.starttls.enable", "true");
                    props.put("mail.smtp.auth", "true");
                    props.put("mail.smtp.host", "smtp.gmail.com");
                    props.put("mail.smtp.port", "587");
                    Session session = Session.getInstance(props,
                            new javax.mail.Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication(username, password);
                                }
                            });
                    try {
                        Message message = new MimeMessage(session);
                        message.setFrom(new InternetAddress("schroniskojava@gmail.com"));
                        message.setRecipients(Message.RecipientType.TO,
                                InternetAddress.parse(email));
                        message.setSubject("eShelter");
                        message.setText("As soon as the end of the capacity of the shelter has just been less than 5 places,"
                                + "\n\n No spam to my email, please!");





                        Transport.send(message);

                        System.out.println("Done");
                        setflagMail="1";
                    } catch (MessagingException e) {
                        throw new RuntimeException(e);
                    }


                }
                // Building Parameters
                HashMap<String, String> paramsMail = new HashMap<>();
                paramsMail.put(TAG_FLAG, setflagMail);
                // sending modified data through http request
                // Notice that flag mail  url accepts POST method
                JSONObject json3 = jsonParser.makeHttpRequest(url_update_flag_mail,
                        "POST", paramsMail);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all animals
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            AllAnimalsActivity.this, animalsList,
                            R.layout.list_item, new String[] { TAG_ID, TAG_SIZE,
                            TAG_NAME,TAG_AGE,TAG_TYPE,TAG_SEX,TAG_HEALTH},
                            new int[] { R.id.id,R.id.number, R.id.name,R.id.age,R.id.type,R.id.sex,R.id.health});
                    // updating listview
                    setListAdapter(adapter);

                }
            });

        }

    }


}
